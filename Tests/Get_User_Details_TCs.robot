#####  Running Commands  ########
# robot   -d    Results  -i       Happy                Tests
# robot   -d    Results  -i       invalid              Tests
# robot   -d    Results  -i       get_all_user         Tests
# robot   -d    Results  -i       get_delphine_info    Tests


*** Settings ***

Resource    ../Resources/common.robot
Resource    ../Resources/PO/search_users.robot


*** Test Cases ***

Get all user details
   [Tags]   Happy   json_placeholder     get_all_user
   ${resp}  Get all users
   Should Be Equal As Strings    ${resp.status_code}   200

Get all "Delphine" user details
   [Tags]   Happy   json_placeholder   get_delphine_info
   ${resp}  Get user details    9
   Should Be Equal As Strings    ${resp.status_code}   200