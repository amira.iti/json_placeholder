#####  Running Commands  ########
# robot   -d    Results  -i       Happy                Tests
# robot   -d    Results  -i       invalid              Tests
# robot   -d    Results  -i       get_all_posts         Tests
# robot   -d    Results  -i       get_delphine_post    Tests
# robot   -d    Results  -i       posts                 Tests


*** Settings ***

Resource    ../Resources/common.robot
Resource    ../Resources/PO/search_posts.robot
Library     ../Libraries/EmailValidator.py

*** Test Cases ***

Get all posts
   [Tags]   Happy   json_placeholder     get_all_post   posts
   ${resp}  Get all posts
   Should Be Equal As Strings    ${resp.status_code}   200

Get user posts
   [Tags]   Happy   json_placeholder   get_delphine_post    posts
   ${resp}     ${post_id}  Get posts by user   9
   Should Be Equal As Strings    ${resp.status_code}   200

Get all comments
    [Tags]   Happy   json_placeholder       all_comments
     ${resp}      ${email}  Get all comments
    Should Be Equal As Strings    ${resp.status_code}   200

Get user comments and validate the e-mail
    [Tags]   Happy   json_placeholder       validate_email
    ${resp}     ${list_post_id}  Get posts by user   9
    ${resp}        Get user comments lists and validate e-mail  ${list_post_id}
    Should Be Equal As Strings    ${resp.status_code}   200