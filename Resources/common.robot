*** Settings ***

###############  Libraries ################
Library     String
Library		Collections
Library     RequestsLibrary
Library     FakerLibrary
Library     DateTime
Library	    JSONLibrary
Library     OperatingSystem


*** Variables ***


############ URLS for Test Environment   #############

${BASE_URL}     https://jsonplaceholder.typicode.com

*** Keywords ***


Set requests headers
   ${headers}=  Create Dictionary  accept=application/xml  Content-Type=application/json
   log  ${headers}
   [Return]  ${headers}


