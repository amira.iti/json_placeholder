
*** Settings ***
Resource     ../common.robot

*** Variables ***


*** Keywords ***


Get posts by user
    [Arguments]     ${id}
    Create session    sessionname     ${BASE_URL}
    ${resp}=  get request  sessionname   /posts?userId=${id}
    log    ${resp.text}
    ${list_post_id}=  Get Value From Json    ${resp.json()}     $..id
    [Return]    ${resp}      ${list_post_id}

Get user comments
    [Arguments]     ${post_id}
    Create session    sessionname     ${BASE_URL}
    ${resp}=  get request  sessionname   /comments?postId=${post_id}
#    log    ${resp.text}
    ${jsondata}=  To Json         ${resp.content}
    ${email}=  Get Value From Json    ${resp.json()}     $..email
    [Return]    ${resp}      ${email}

Validate e-mail format
     [Arguments]     ${post_id}     ${email}
                  FOR    ${i}      IN     @{email}
                          check_email    ${i}
                      END

Get user comments lists and validate e-mail
    [Arguments]     ${list_post_id}
    Create session    sessionname     ${BASE_URL}
                      FOR    ${i}      IN     @{list_post_id}
                             ${resp}      ${email}  Get user comments   ${i}
                             Validate e-mail format     ${i}    ${email}
                          END
    [Return]    ${resp}


#####################

Get all posts
    Create session    sessionname     ${BASE_URL}
    ${resp}=  get request  sessionname  /posts
    log    ${resp.text}
    ${jsondata}=  To Json         ${resp.content}
    log    ${jsondata}
    [Return]    ${resp}


Get all comments
    [Arguments]     ${post_id}
    Create session    sessionname     ${BASE_URL}
    ${resp}=  get request  sessionname   /comments
    log    ${resp.text}
    ${jsondata}=  To Json         ${resp.content}
    ${email}=  Get Value From Json    ${resp.json()}     $..email
    [Return]    ${resp}      ${email}

