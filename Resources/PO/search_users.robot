
*** Settings ***
Resource     ../common.robot

*** Variables ***


*** Keywords ***

Get all users
    Create session    sessionname     ${BASE_URL}
    ${resp}=  get request  sessionname  /users
    log    ${resp.text}
    [Return]    ${resp}

Get user details
    [Arguments]     ${id}
    Create session    sessionname     ${BASE_URL}
    ${resp}=  get request  sessionname  /users/${id}
    log    ${resp.text}
    ${jsondata}=  To Json         ${resp.content}
    ${user_id}=  Set Variable  ${jsondata['id']}
    log  ${user_id}
#    ${user_id}=  Convert to string  ${username}
    [Return]    ${resp}