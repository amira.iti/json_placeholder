# JSON Placeholder REST API using Robot Framework

# Why Robot Framework

- Open source
- Very easy to install
- RF is application and platform independent
- User doesn't need a programming language to write a Robot Framework test case & run it
- Supports Keyword driven, Data-driven and Behaviour-driven (BDD) approaches
- The use of Selenium2 library in RF will help  to automate the existing manual test cases of a project in a Rapid Phase
- Outstanding Reporting system: The RF automatically generates a report and html log after executing each build
- Robot Framework provides lots of libraries to test different application like Appium Library for Mobile Automation, Database Library for DB testing, Android Library etc.


## Key Feature List

- #### Get User Details
- #### Search for Posts written by a User


## How to install Robot Framework with Python


- Robot Framework is implemented with Python, so you need to have Python installed.
On Windows machines, make sure to add Python to PATH during installation.

- Installing Robot Framework with pip is simple:

`pip install robotframework`

- To check that the installation was succesful, run

`robot --version`

- For a full guide, please see Installation instructions. It also covers topics such as running Robot Framework on Jython (JVM) and IronPython (.NET).

**Now you are ready to write your first tests!**

## Other Dependencies

```
pip install robotframework
pip install robotframework-faker
pip install robotframework-SeleniumLibrary
pip install robotframework-jsonlibrary
pip install robotframework-requests
```


## Robot Framework Project structure

- ###### Request Body folder that contains all json data for all requests
- ###### Tests folder that contains all test cases
- ###### Resources folder that will include all common and all page object pages
- ###### Results Folder that all have the result report along with the tags


## Robot Framework Test Cases

- #### Get User Details

```

```



## Run the Test

### To run all test cases
` robot   -d    Results  -i       json_placeholder         Tests`


### To run all happy scenarios test cases
`robot   -d    Results  -i       Happy            Tests`


### To run all invalid scenarios test cases
`robot   -d    Results  -i       invalid            Tests`
